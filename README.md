# lset

An ultralightweight sets library for Lua. Create, compare, and operate on sets.

## Usage

Simply add this to your Lua script:

```lua
set = require "lset"
```

From then on, you can create sets by calling `set` either with no parameters or a table in list form with the entries to
your set.

### Operations

The following operators are supported. Assume `x` and `y` are set variables:

- `#x` for the cardinality of `x`
- `x - y` for set subtraction (i.e. all elements of `x` that are not also in `y`)
- `x | y` for set union (i.e. all elements of either set)
- `x & y` for set intersection (i.e. elements present in both sets)
- `x ~ y` for set XOR (i.e. elements in only one of the two sets)
- `x * y` for the Cartesian product (i.e. all ordered pairs `(x, y)` from the elements of `x` and `y`)
- `x == y` for set equality (i.e. whether both sets contain only the same elements)
  + `x ~= y` is identical to `not (x == y)`
- `x <= y` for set containment (i.e. whether all elements of `x` are elements of `y`)
  + `x >= y` is identical to `y <= x`
- `x < y` for strict set containment (i.e. whether all elements of `x` are elements of `y` *and* there is at least one
element of `y` that is not an element of `x`)
  + `x > y` is identical to `y < x`

The main `set` table (returned by `require "lset"`) contains two helper functions: `aslist` and `power`. Each of those
accepts one set object as an argument. `aslist` returns the set in list form, i.e. integers 1 to `#x` inclusive are keys
and the values are the elements of the set. The list's order is not predictable. `power` returns the power set of its
argument as a set object, i.e. the set of all its subsets.

The only noteworthy missing operator is `~x` for set complement, simply because I cannot find a way to implement it
without relying on a universal set. I may add support for this when a universal set is provided, but there are two
competing ways to go about that and I don't like either of them.

### Expanding sets

You can add an element to a set at will. If `x` is a set, then `x[elem] = true` will add `elem` to the set. Of course,
that element can be any value in Lua. The set's cardinality will work as expected.

**Note that you are not able to contract sets. Even** `x[elem] = nil` **will not completely work, as set cardinality
will not be subtracted. As far as I know, *this is unfixable.***

## Examples

Creating the empty set: `x = set()` or `x = set{}`  
Creating a set of all single-digit positive integers: `x = set{1, 2, 3, 4, 5, 6, 7, 8, 9}`  
Verifying that `x` is a superset of `y`: `assert(x >= y)`  
Adding two integers to an empty set: `x = set(); x[5] = true; x[-2] = true`  

## Final words

You can check the version of this library you have installed at any time by checking `set._VERSION`  
There also exists `set._HEXVERSION` which is a 4-byte integer constant you can use to compare versions:

1. the highest byte is the major version,
2. then the minor,
3. then the patch,
4. and finally `0xff` for release versions, or some other number during development (usually iterating from zero)

The latest version is 1.2.1
