local function set(s)
	if s then assert(type(s) == "table") end
	local mt = {}

	--Set cardinality is kept track of at all times
	--Time complexity: Θ(1)
	mt.n = 0
	mt.__len = function(s) return mt.n end
	mt.__newindex = function(t, k, v)
		if not v then return end
		mt.n = mt.n + 1
		rawset(t, k, v)
	end

	--Subtraction: A - B = elements of A not in B
	--Time complexity: Θ(|A|)
	mt.__sub = function(l, r)
		local out = set()

		for k in pairs(l) do
			if not r[k] then out[k] = true end
		end

		return out
	end

	--Cartesian product: all pairs (a, b) of elements from A and B
	--Pairs are tables where a occupies index 1 and b index 2
	--Time complexity: Θ(|A| |B|)
	mt.__mul = function(l, r)
		local out = set()

		for a in pairs(l) do
			for b in pairs(r) do
				out[{a, b}] = true
			end
		end

		return out
	end

	--Intersection: A & B = elements of both A and B
	--Time complexity: Θ(|A|) or Θ(|B|), whichever is faster
	mt.__band = function(l, r)
		local out = set()

		local small = #l <= #r and l or r
		local large = #l <= #r and r or l
		for k in pairs(small) do
			if large[k] then out[k] = true end
		end

		return out
	end

	--Union: A | B = elements of A and elements of B
	--Time complexity: Θ(|A| + |B|)
	mt.__bor = function(l, r)
		local out = set()

		for k in pairs(l) do
			out[k] = true
		end

		for k in pairs(r) do
			out[k] = true
		end

		return out
	end

	--XOR: A ~ B <=> (A - B) | (B - A)
	--Time complexity: Θ(|A| + |B|)
	mt.__bxor = function(l, r)
		local out = set()

		for k in pairs(l) do
			if not r[k] then out[k] = true end
		end

		for k in pairs(r) do
			if not l[k] then out[k] = true end
		end

		return out
	end

	--Equality: A = B iff all elements of A are elements of B and vice versa
	--Time complexity: O(|A|) and Ω(1)
	mt.__eq = function(l, r)
		if #l ~= #r then return false end

		for k in pairs(l) do
			if not r[k] then return false end
		end

		return true
	end

	--Containment: A <= B <=> A & B == A
	--Time complexity: O(|A|)
	mt.__le = function(l, r)
		for k in pairs(l) do
			if not r[k] then return false end
		end

		return true
	end

	--Strict containment: A < B <=> A <= B and A ~= B
	--Time complexity: O(|A|) and Ω(1)
	mt.__lt = function(l, r)
		if #l == #r then return false end

		for k in pairs(l) do
			if not r[k] then return false end
		end

		return true
	end

	--Build the set from input, if provided
	local out = setmetatable({}, mt)
	if s then
		for _, v in ipairs(s) do
			out[v] = true
		end
	end

	return out
end

--Make a set into a list
local function aslist(s)
	local setlist = {}
	for k in pairs(s) do
		table.insert(setlist, k)
	end

	return setlist
end

--Build the power set iteratively
--https://sevko.io/articles/power-set-algorithms/#tocAnchor-1-8
local function power(s)
	local outlist = {set()}
	for k in pairs(s) do
		for i = 1, #outlist do
			local subset = aslist(outlist[i])
			table.insert(subset, k)
			table.insert(outlist, set(subset))
		end
	end

	return set(outlist)
end

return setmetatable({_VERSION = "1.2.1", _HEXVERSION = 0x010201ff, aslist = aslist, power = power},
					{__call = function(_, ...) return set(...) end})
